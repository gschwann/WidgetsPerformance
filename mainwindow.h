#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>

class QGridLayout;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void updateLabels();

private:
    Ui::MainWindow *ui;
    QTimer m_timer;
    int m_tick;
    QGridLayout* m_layout;
};

#endif // MAINWINDOW_H
