#ifndef DATAWIDGET_H
#define DATAWIDGET_H

#include <QWidget>

class DataWidget : public QWidget
{
    Q_OBJECT
public:
    explicit DataWidget(QWidget *parent = nullptr);

signals:

public slots:

protected:

    // QWidget interface
protected:
    void paintEvent(QPaintEvent* event) override;

private:
    int m_tick;
};



#endif // DATAWIDGET_H
