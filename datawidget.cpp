#include "datawidget.h"

#include <QPainter>

DataWidget::DataWidget(QWidget *parent)
    : QWidget(parent)
    , m_tick(0)
{
}

void DataWidget::paintEvent(QPaintEvent* event)
{
    Q_UNUSED(event)
    QString txt = QString::number(m_tick);
    QPainter painter(this);
    for (int y = 0; y < 20; ++y) {
        for (int x = 0; x < 25; ++x) {
            painter.drawText(x*40, y*22, txt);
        }
    }

    ++m_tick;
}
