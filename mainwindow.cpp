#include "datawidget.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QGridLayout>
#include <QLabel>
#include <QtGlobal>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , m_tick(0)
    , m_layout(new QGridLayout())
{
    ui->setupUi(this);

//    for (int y = 0; y < 20; ++y) {
//        for (int x = 0; x < 25; ++x) {
//            m_layout->addWidget(new QLabel(centralWidget()), x, y);
//        }
//    }

    auto dataItem = new DataWidget(centralWidget());
    m_layout->addWidget(dataItem);

    centralWidget()->setLayout(m_layout);

    updateLabels();

    m_timer.setInterval(100);
//    connect(&m_timer, &QTimer::timeout, this, &MainWindow::updateLabels);
    connect(&m_timer, &QTimer::timeout, dataItem, QOverload<>::of(&DataWidget::update));
    m_timer.start();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateLabels()
{
//    QString txt = QString::number(m_tick);
//    for (int y = 0; y < 20; ++y) {
//        for (int x = 0; x < 25; ++x) {
//            QLayoutItem* item = m_layout->itemAtPosition(x, y);
//            QWidget* widget = item->widget();
//            QLabel* label = qobject_cast<QLabel*>(widget);
//            label->setText(txt);
//        }
//    }

//    ++m_tick;
}
